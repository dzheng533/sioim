/*
*
*/

var http = require('http');//.createServer(handler);
var socketIO = require('socket.io');
var ConstDef = require('./commonDef.js');
/**
* Module exports.
*/
module.exports = SIOServer;

var config={
	'port':3129
}

function SIOServer(){
   this.version = "1.0.0";
   this.app = http.createServer(this.httpHandler);
   this.io = new socketIO(this.app);
}

SIOServer.prototype.httpHandler = function(req,res) {
	res.writeHead(200);
	res.end("SIOServer is running.");
}

SIOServer.prototype.start = function(configuration){
   console.log(this.version);
   config = configuration==null?config:configuration;

   this.app.listen(config.port);
   console.log('SIO Server start at %s.',config.port);

   console.log(ConstDef.EVNET_MESSAGE);

   this.io.on(ConstDef.EVENT_NEW_CONNECTION, function (socket) {
   	console.log("New Client connected.");
      socket.emit(ConstDef.EVNET_MESSAGE, { hello: 'world' });
      socket.on(ConstDef.EVENT_CLIENT_LOGIN, function (data) {
        console.log(data);
        if(!data.sessionId){
          socket.emit(ConstDef.EVENT_SERVER_ASSIGNSESSION,{sessionId:"12345678909876543"})
        }
       });
   });

   this.io.on('disconnect',function(socket){

       console.log("Client disconnected.");
   });
};