/*
* utils
*/
var fs=require('fs');  
var path = require('path');

exports.parseConfig = function (configFile,callback){
	if(configFile == null){
		if(callback)
	    	callback(null);
    }else{
		fs.readFile(configFile,function(err,data){  
	    	if(err)  
	        	throw err;
	    	var jsonObj=JSON.parse(data);
	    	if(callback)
	    		callback(jsonObj);
		});
    }
}


exports.writeConfig = function (configFile,config){

    var strConfig = JSON.stringify(config);
    var viewRootPath = path.resolve(configFile);
    fs.writeFile(viewRootPath,strConfig,function(err){  
        if(err) throw err;  
        console.log('write Config to :%s',viewRootPath);  
    });
}