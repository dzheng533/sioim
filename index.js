/*
*
*/

var sioServer = require("./lib/server.js");
var sioClient = require("./lib/client.js");
var Utils = require("./lib/utils.js");

var sio = null;
var args = process.argv;
var argCount = process.argv.length;

//console.log('Count:' + argCount);
if(argCount >= 3){
	var type = args[2];
	var configFile = null;
	if(argCount >=4){
		configFile = args[3];
		console.log("Loading config file:"+configFile);
	}
	//Load config file
    var config = null;
	if(configFile!= null){
		Utils.parseConfig(configFile,function (config){

		if(type == 'Server')
		  startServer();
	    else if(type == 'Client')
		  startClient(config);
	    else
		  console.log('Bad Argument.\rUsage: node index.js <Server|Client> [Config file]');
		});
	}
}else{
	console.log('Usage: node index.js <Server|Client> [Config file]');
}

function startServer(){
  console.log('Start Server.');
  sio = new sioServer();
  sio.start();
}

function startClient(config){
  console.log('Start Client.');
  var defaultConfig = {"port":3129,"server":'localhost'};
  sio = new sioClient();
  if(config){
  	console.log('Re-Write default config.');
  	defaultConfig = config;
  }
  sio.connect(defaultConfig);
  
}